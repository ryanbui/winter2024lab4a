public class Student {

	private String name;
	private double rScore;
	private String program;
	private int amountLearnt;

	public Student(String name, String program) {
		this.name = name;
		this.rScore = 0;
		this.program = program;
		this.amountLearnt = 0;
	}

	//setter methods
	public void setRScore(double newRScore) {
		this.rScore = newRScore; 
	   }
	public void setAmountLearnt(int newAmountLearnt) {
		this.amountLearnt = newAmountLearnt; 
	   }
		  
	   
	//getter methods
	public String getName() {
		return this.name;
	}
	public double getRScore() {
		return this.rScore;
	}
	public String getProgram() {
		return this.program;
	}
	public int getAmountLearnt() {
		return this.amountLearnt;
	}
	
	public void greetingToCS() {
		if(this.program.equals("Computer Science Technology")) {
			System.out.println(name + " go touch some grass you nerd"+ ".");
		} else {
			System.out.println("Hi " + name + ". " + "I hope you are having fun in " + program + ".");
		}
	}
	
	public void rateStudentRScore() {
		if(this.rScore <= 20.0){
			System.out.println("Your R Score is " + rScore +". Consider dropping out" + ".");
		} else if(this.rScore > 20.0 && this.rScore <= 25.0){
			System.out.println("Your R Score is " + rScore +". Would you maybe like some help?" + ".");
		} else if(this.rScore > 25.0 && this.rScore <= 30.0){
			System.out.println("Your R Score is " + rScore +". Good job!"+ ".");
		} else {
			System.out.println("Your R Score is " + rScore +". DAMN YOU ARE INSANE!"+ ".");
		}
	}

	public void study(int amountStudied) {
		amountLearnt += amountStudied;
	}
}