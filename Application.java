import java.util.Scanner;
public class Application {

	public static void main (String [] args){
		Scanner reader = new Scanner(System.in);
		Student student1 = new Student("Ryan", "Computer Science Technology");
		student1.setRScore(34.9);
		
		Student student2 = new Student("Jason", "Pure and Applied Science");
		student2.setRScore(33.8);
		System.out.println(student1.getName()); // prints Ryan
		System.out.println(student1.getRScore());
		System.out.println(student1.getProgram());
		System.out.println(student2.getName());
		System.out.println(student2.getRScore());
		System.out.println(student2.getProgram());
		
		Student [] section3 = new Student[3];
		section3[0] = student1;
		section3[1] = student2;
		
		section3[2] = new Student("Nate", "ALC" );
		section3[2].setRScore(32.0);
		
		System.out.println(section3[2].getName());
		System.out.println(section3[2].getProgram());
		System.out.println(section3[2].getRScore());
		
		student1.greetingToCS();
		student1.rateStudentRScore();
		student2.greetingToCS();
		student2.rateStudentRScore();

		System.out.println("");
		System.out.println(section3[0].getAmountLearnt());
		System.out.println(section3[1].getAmountLearnt());
		System.out.println(section3[2].getAmountLearnt());
		System.out.println("");
		System.out.println("Please enter an amount studied");
		int amountStudied = reader.nextInt();
		
		section3[2].study(amountStudied);
		section3[2].study(amountStudied);
		section3[2].study(amountStudied);      

		System.out.println("");
		System.out.println(section3[0].getAmountLearnt());
		System.out.println(section3[1].getAmountLearnt());
		System.out.println(section3[2].getAmountLearnt());

		Student student4 = new Student("Caden", "Computer Science Technology");
		student4.setRScore(32.4);
		System.out.println("");
		System.out.println(student4.getName());
		System.out.println(student4.getRScore());
		System.out.println(student4.getProgram());
		System.out.println(student4.getAmountLearnt());


		


		
		
	}
}